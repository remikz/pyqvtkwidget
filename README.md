![Alt text](example.png?raw=true "Example")

Overview
========
PyQVTKWidget allows you to create any number of VTK render windows in a 
Python Qt program.

Motivation
==========
There are many Python Qt examples with QVTKRenderWindowInteractor, however,
you may have experienced either of these limitations:
* segmentation fault if not executed in a specific order
* no automatic resizing unless parented as a central main window widget
* no visibility unless parented by the main window widget
* tight coupling with main window widget and less modular design

Credits
=======
This widget heavily re-uses code from the VisTrails project.

License
=======
Copyright (C) 2016  Remik Ziemlinski

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
