#!/usr/bin/env python

from PyQVTKWidget import PyQVTKWidget
from PyQt4 import QtCore, QtGui

import sys
import vtk

class WidgetManager:
    def __init__(self, ui):
        self.ui = ui    
        self.createCenterWidget()
        self.createDockWidgets()
        
    def createCenterWidget(self):
        self.centerWidget = QtGui.QTextEdit()
        self.centerWidget.setText('centerWidget '*50)
        self.ui.setCentralWidget(self.centerWidget)

    def createDockWidget(self, i, name):
        dock = QtGui.QDockWidget('dockWidget' + str(i), self.ui)
        self.dockWidgets.append(PyQVTKWidget())
        dock.setWidget(self.dockWidgets[-1])
        self.ui.addDockWidget(QtCore.Qt.RightDockWidgetArea if i % 2 else \
                              QtCore.Qt.LeftDockWidgetArea, 
                              dock)

        self.dockWidgets[-1].show()
        self.dockWidgets[-1].setMinimumSize(200, 150)
        self.dockWidgets[-1].Render()

        source = eval('vtk.vtk' + name + 'Source()')
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(source.GetOutputPort())
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        self.dockWidgets[-1].mRen.AddActor(actor)
        self.dockWidgets[-1].mRen.ResetCamera()
        
    def createDockWidgets(self):
        self.dockWidgets = []
        for i, name in enumerate('Arrow Cone Cube Cylinder Sphere'.split()):
            self.createDockWidget(i, name)

        
class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setWindowTitle("Example") 
        self.setMinimumSize(800, 600)       
        self.show()    
                

if '__main__' == __name__:
    app = QtGui.QApplication(sys.argv)
    ui = MainWindow()
    widgets = WidgetManager(ui)
    sys.exit(app.exec_())